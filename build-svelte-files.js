const fs = require('fs')
const path = require('path')
const jsdom = require('jsdom')
const { JSDOM } = jsdom

const folderName = './node_modules/heroicons/outline'
const isFile = file => {
    return fs.lstatSync(file.path).isFile()
}
const camelCase = str => {
    return str.split('-').map((d, i) => i === 0
        ? d
        : [...d].map((d, i) => i === 0
            ? d.toUpperCase()
            : d).join('')
    ).join('')
}
function walkTree(node) {
    let n = {}
    console.log(typeof node)
    console.log(node.length)
}
try {
    if (fs.existsSync(folderName)) {
        let files = fs.readdirSync(folderName)
            .filter(file => file.includes('.svg'))
            // .map(file => file)
            .map(fileName => ({
                path: path.join(folderName, fileName),
                newPath: path.join('./src/icons', fileName).replace('.svg', '.js'),
                fileName: fileName.replace('.svg', ''),
                camelName: camelCase(fileName.replace('.svg', ''))
            }))
            .filter(isFile)
            // .filter((d, i) => i <= 1)
            .map(d => {
                console.log(d)
                return d
            })
            .map(d => {
                // return 0
                const data = fs.readFileSync(d.path, 'utf8')
                let dom = new JSDOM(data)
                let svgs = dom.window.document.querySelectorAll('svg')
                if (svgs.length == 1) {
                    let contents
                    let paths = dom.window.document.querySelectorAll('path')
                    svgs = svgs[0]
                    console.log(svgs.attributes)
                    console.log(svgs.children)
                    // return 0
                    for (let name of svgs.getAttributeNames()) {
                        let value = svgs.getAttribute(name)
                        console.log({ name, value })
                    }
                    // return 0
                    let ds = []
                    paths.forEach(p => {
                        ds.push(p.getAttribute('d'))
                    })
                    console.log(ds)
                    contents = `export const ${d.camelName} = ${JSON.stringify(ds, null, 2)}`
                    console.log(contents)
                    fs.writeFile(d.newPath, contents, err => err ? console.log(err) : '')
                    return ({ contents, ...d })
                } else {
                    console.log('nodelist count')
                    return 0
                }
            })
        let imports = files.map(d => `import { ${d.camelName} } from "./icons/${d.fileName}";`).join('\n')
        let exports = files.map(d => d.camelName).join(',\n    ')
        let contents = `${imports}\nexport {${exports}};`
        fs.writeFile(path.join('./src/', 'exports.js'), contents, err => err ? console.log(err) : '')
    }
} catch (err) {
    console.error(err)
}