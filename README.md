# Heroicons ready for svelte

This are the transpiled icon files to be used in svelte aplications, they are threeshake friendly as you import what is needed. Uses [Heroicons](https://github.com/tailwindlabs/heroicons)

## Setting up

Import it with `npm i -D @4mende2/svelte-heroicons`

## Usage

```html
<script>
  import Icon, {clock} from "@4mende2/svelte-heroicons";
</script>

<Icon icon={clock} class="h-6 w-6 text-blue-600" />
```

## Consuming components

Your package.json has a `"svelte"` field pointing to `src/index.js`, which allows Svelte apps to import the source code directly, if they are using a bundler plugin like [rollup-plugin-svelte](https://github.com/sveltejs/rollup-plugin-svelte) or [svelte-loader](https://github.com/sveltejs/svelte-loader) (where [`resolve.mainFields`](https://webpack.js.org/configuration/resolve/#resolve-mainfields) in your webpack config includes `"svelte"`). **This is recommended.**

For everyone else, `npm run build` will bundle your component's source code into a plain JavaScript module (`dist/index.mjs`) and a UMD script (`dist/index.js`). This will happen automatically when you publish your component to npm, courtesy of the `prepublishOnly` hook in package.json.
